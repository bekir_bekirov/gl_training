/*
 * coder.h
 *
 *  Created for: GlobalLogic Education
 *       Author: vitalii.lysenko
 *
 * Coder class header.
 *
 * You have to change this file to fix build errors, and make
 * the unit tests passed.
 *
 */

#ifndef CODER_H
#define CODER_H

#include <memory>
#include <iostream>
#include <cstring>
#include <limits> 

/*
 * This function was pre-compiled and is provided as a part of the
 * static library.
 *
 */
extern void encode(char* buf, int size);

/*
 * Coder class header.
 *
 */
class Coder
{
private:
	char *m_buf;
	unsigned int m_size;

public:
	explicit Coder(): m_buf(nullptr), m_size(0) {}
	~Coder();
	Coder(const Coder &rhs);
	Coder& operator= (const Coder &rhs);

	const char *buf()
	{
		return m_buf;
	}

	int size()
	{
		return m_size;
	}

	void encode();
	void decode();
	void set(const char* buf, unsigned int len);
};

#endif // CODER_H
