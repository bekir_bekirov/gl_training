/*
 * coder.cpp
 *
 *  Created for: GlobalLogic Education
 *       Author: vitalii.lysenko
 *
 * Coder class source.
 *
 * You may have to change this file to fix build errors, and make
 * the unit tests pass.
 *
 * Usually source files are used to write an implementation of
 * global and member functions.
 *
 */

#include "coder.h"

/*
 * To make all unit tests pass you may try to puzzle out the ::encode() algorithm
 * and it should help you to write the decoding one. But there are other ways to
 * make them pass.
 *
 * Good luck!
 *
 */

Coder::~Coder()
{
	delete[] m_buf;
	m_buf = 0;
	m_size = 0;
}

Coder::Coder(const Coder &rhs)
{
	this->m_size = rhs.m_size;
	this->m_buf = new char[rhs.m_size];
	memcpy(this->m_buf, rhs.m_buf, rhs.m_size);
}

Coder& Coder::operator= (const Coder &rhs)
{
	if (this == &rhs) return *this;

	delete[] this->m_buf;
	this->m_size = rhs.m_size;
	this->m_buf = new char[rhs.m_size];
	memcpy(this->m_buf, rhs.m_buf, rhs.m_size);

	return *this;
}

void Coder::encode()
{
	::encode(m_buf, m_size);
}

void Coder::decode()
{
	m_buf[m_size - 1] ^= 0xFF;
	for(int i = m_size - 2; i >= 0; i--) {
		m_buf[i] ^= m_buf[i + 1];
	}
}

void Coder::set(const char* buf, unsigned int len)
{
	if (len == 0) {
		throw std::logic_error("Length buffer cannot be 0!");
	}
	if (len >= std::numeric_limits<unsigned int>::max()) {
		throw std::logic_error("Buffer is too long!");
	}
	if ((buf == nullptr) || (buf == 0)) {
		throw std::logic_error("Null ptr used to initialize buffer!");
	} 
	
	this->m_buf = new char[len];
	this->m_size = len;
	memcpy(this->m_buf, buf, len);
}
